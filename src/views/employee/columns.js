import React from 'react';
import { Row } from "antd";

export const columns = [

    {
        dataIndex: 'fullName',
        title: 'Full Name',
    },
    {
        dataIndex: 'phoneNumber',
        title: 'Phone Number',
    }
];
