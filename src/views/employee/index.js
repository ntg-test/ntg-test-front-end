import React, { useEffect, useState } from "react";

import { Table, Row, Button, Spin, Card } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { columns } from './columns';
import AddEmployeeModal from './add-modal';
import * as EmployeeServices from '../../services/employee/index';

function Employee() {
    const [isModalVisible, setModalVisible] = useState(false);
    const [spinning, setSpinning] = useState(true);
    const [employees, setEmployees] = useState([]);
   
    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        setSpinning(true);
        (async () => {
        const data = await EmployeeServices.showAllEmployee();
        setEmployees(data.data);
        setSpinning(false);
        })();
    };
    const onFinish = (values) => {

        (async () => {
            await EmployeeServices.addEmployee(values);
            getData();
        })();
    };

    return (
        <>
            <div className="content">
                <Card style={{ minHeight: '85vh', borderRadius: '5px' }}>
                    <Spin spinning={spinning} >
                        <Row justify='end' align='middle'>
                            <Button type='primary' onClick={() => {
                                setModalVisible(true);
                            }} >
                                <Row align='middle'>
                                    <PlusOutlined /> Assign Job
                                </Row>
                            </Button>
                        </Row>
                        <Row>
                            <Table dataSource={employees} columns={[...columns]} style={{
                                width: '100%',
                                padding: ' 16px 0 0',
                                borderRadius: '7px'
                            }} />
                        </Row>
                        <AddEmployeeModal
                            isVisible={isModalVisible}
                            setVisible={setModalVisible}
                            addEmployee={onFinish}
                            employees={employees}
                        />
                    </Spin>
                </Card>
            </div>
        </>
    );
}

export default Employee;
