import React, { useState } from 'react';
import { Row, Modal, Button, Form, Input, Col, Select } from 'antd';
import TextArea from 'antd/lib/input/TextArea';

const AddEmployeeModal = ({ isVisible, setVisible, employees }) => {
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const [selectedItems, setSelectedItems] = useState([]);

    const onFinish = (values) => {
    };

    return (
        <>
            <Modal
                title={'Assign Job'}
                visible={isVisible}
                onCancel={() => { setVisible(false); form.resetFields(); }}
                okButtonProps={{ hidden: true }}
                cancelButtonProps={{ hidden: true }}
                width={675}
            >
                <Form
                    form={form}
                    layout='vertical'
                    onFinish={onFinish}
                >
                    <Row gutter={24} justify='space-between'>
                        <Col sm={24} lg={12}>

                            <Form.Item
                                label="Employee"
                                name="employeeId"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please Add Employee!',
                                    },
                                ]}
                            >
                                <Select
                                    mode="multiple"
                                    placeholder="Inserted are removed"
                                    value={selectedItems}
                                    onChange={setSelectedItems}
                                    style={{ width: '100%' }}
                                >
                                    {employees.map(employee => (
                                        <Select.Option key={employee.id} value={employee.id}>
                                            {employee.fullName}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={24} justify='space-between'>
                        <Col sm={24} lg={24}>
                            <Form.Item
                                label="Job Description"
                                name="job_description"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please Add Job Description!',
                                    },
                                ]}
                            >
                                <TextArea />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row justify='end'>
                        <Col style={{ margin: '0 8px 0 0' }}>
                            <Form.Item >
                                <Button htmlType="button" onClick={() => {
                                    setVisible(false);
                                    form.resetFields();
                                }}>
                                    Cancel
                                </Button>
                            </Form.Item>
                        </Col>
                        <Form.Item>
                            <Col>
                                <Button loading={loading} disabled={loading} type="primary" htmlType="submit">
                                    Save
                                </Button>
                            </Col>
                        </Form.Item>
                    </Row>
                </Form>
            </Modal>
        </>
    );

};

export default AddEmployeeModal;