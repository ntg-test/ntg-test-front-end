import Employee from "views/employee/index";

var routes = [    
  {
    path: "/",
    name: "home",
    rtlName: "",
    icon: "tim-icons icon-single-02",
    component: Employee,
    layout: "/admin",
  },

];

export default routes;
