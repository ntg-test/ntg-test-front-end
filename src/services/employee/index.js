/* eslint-disable no-useless-concat */
import http from '../common/http/index';
import AppConsts from '../../app-consts';

const apiEndpoint = AppConsts.remoteServiceBaseUrl ;

export async function addEmployee(addEmployee) {
    const data = await http.post(apiEndpoint + 'add', addEmployee);
    return data;
}

export async function showAllEmployee() {
    const data = await http.get(apiEndpoint + 'employees');
    return data;
}

